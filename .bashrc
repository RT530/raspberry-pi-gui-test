#!/bin/sh

if [ "$TERM" == "linux" ]; then
    wget -q --spider http://google.com

    if [ $? -eq 0 ]; then
        cd /home/gui/raspberry-pi-gui-test
        git fetch
        if [ $(git rev-parse HEAD) != $(git rev-parse @{u}) ]; then
            echo "Found a new version, updating..."
            git pull --force
            sudo reboot
        fi
    fi
    startx
fi
