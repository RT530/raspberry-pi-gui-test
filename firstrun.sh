#!/bin/bash

set +e

# set hostname to "GUI"
CURRENT_HOSTNAME=`cat /etc/hostname | tr -d " \t\n\r"`
if [ -f /usr/lib/raspberrypi-sys-mods/imager_custom ]; then
   /usr/lib/raspberrypi-sys-mods/imager_custom set_hostname GUI
else
   echo GUI > /etc/hostname
   sed -i "s/127.0.1.1.*$CURRENT_HOSTNAME/127.0.1.1\tGUI/g" /etc/hosts
fi

FIRSTUSER=`getent passwd 1000 | cut -d: -f1`
FIRSTUSERHOME=`getent passwd 1000 | cut -d: -f6`
if [ -f /usr/lib/raspberrypi-sys-mods/imager_custom ]; then
   /usr/lib/raspberrypi-sys-mods/imager_custom enable_ssh
else
   systemctl enable ssh
fi

# create user
if [ -f /usr/lib/userconf-pi/userconf ]; then
   /usr/lib/userconf-pi/userconf 'gui' '$5$20IuJol6sK$a/eKT/IjQ3wRm51v7RANMi1TplH4FKcUXBZcJtN/XJA'
else
   echo "$FIRSTUSER:"'$5$20IuJol6sK$a/eKT/IjQ3wRm51v7RANMi1TplH4FKcUXBZcJtN/XJA' | chpasswd -e
   if [ "$FIRSTUSER" != "gui" ]; then
      usermod -l "gui" "$FIRSTUSER"
      usermod -m -d "/home/gui" "gui"
      groupmod -n "gui" "$FIRSTUSER"
      if grep -q "^autologin-user=" /etc/lightdm/lightdm.conf ; then
         sed /etc/lightdm/lightdm.conf -i -e "s/^autologin-user=.*/autologin-user=gui/"
      fi
      if [ -f /etc/systemd/system/getty@tty1.service.d/autologin.conf ]; then
         sed /etc/systemd/system/getty@tty1.service.d/autologin.conf -i -e "s/$FIRSTUSER/gui/"
      fi
      if [ -f /etc/sudoers.d/010_pi-nopasswd ]; then
         sed -i "s/^$FIRSTUSER /gui /" /etc/sudoers.d/010_pi-nopasswd
      fi
   fi
fi

#set timezone
if [ -f /usr/lib/raspberrypi-sys-mods/imager_custom ]; then
   /usr/lib/raspberrypi-sys-mods/imager_custom set_keymap 'us'
   /usr/lib/raspberrypi-sys-mods/imager_custom set_timezone 'Pacific/Auckland'
else
   rm -f /etc/localtime
   echo "Pacific/Auckland" >/etc/timezone
   dpkg-reconfigure -f noninteractive tzdata
cat >/etc/default/keyboard <<'KBEOF'
XKBMODEL="pc105"
XKBLAYOUT="us"
XKBVARIANT=""
XKBOPTIONS=""
KBEOF
   dpkg-reconfigure -f noninteractive keyboard-configuration
fi

NAME="raspberry-pi-gui-test"
echo "#!/bin/bash" > "/usr/local/bin/upgrade"
echo "" >> "/usr/local/bin/upgrade"
echo "sudo apt update" >> "/usr/local/bin/upgrade"
echo "sudo apt install -y" >> "/usr/local/bin/upgrade"
echo "sudo apt autoremove -y" >> "/usr/local/bin/upgrade"
echo "" >> "/home/gui/install"
chmod +x /usr/local/bin/upgrade

echo "#!/bin/bash" > "/home/gui/install"
echo "" >> "/home/gui/install"
echo "sudo apt update" >> "/home/gui/install"
echo "sudo apt install -y python3-tk xinit git" >> "/home/gui/install"
echo "" >> "/home/gui/install"
echo "git clone https://gitlab.com/RT530/$NAME" >> "/home/gui/install"
echo "" >> "/home/gui/install"
echo "rm /home/gui/.bashrc" >> "/home/gui/install"
echo "" >> "/home/gui/install"
echo "ln -s /home/gui/$NAME/.bashrc" >> "/home/gui/install"
echo "" >> "/home/gui/install"
echo "sudo raspi-config" >> "/home/gui/install"
echo "" >> "/home/gui/install"
echo "rm -f /home/gui/install" >> "/home/gui/install"
echo "exit 0" >> "/home/gui/install"
chmod +x /home/gui/install

echo "#!/bin/bash" > "/home/gui/.xinitrc"
echo "" >> "/home/gui/.xinitrc"
echo "sudo python3 /home/gui/$NAME/main.py" >> "/home/gui/.xinitrc"
chmod +x /home/gui/.xinitrc

rm -f /boot/firstrun.sh
sed -i 's| systemd.run.*||g' /boot/cmdline.txt
exit 0
