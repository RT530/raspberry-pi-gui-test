import tkinter as tk

window = tk.Tk()
window.attributes('-fullscreen', True)
height = window.winfo_screenheight()
width = window.winfo_screenwidth()

window.geometry(f'{width}x{height}')

label = tk.Label(window, text=f'{width}x{height}')
label.pack()

button = tk.Button(window)
button['text'] = 'Click Me'
button['command'] = exit
button.pack()

window.mainloop()
